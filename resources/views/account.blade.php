@extends('layouts.app')

@section('title')
    Главная
@endsection

@section('content-header')
    <h1>
        @if(isset($id)) Изменение @else Добавление @endif аккаунта
    </h1>
    <ol class="breadcrumb">
        <li><a href="/">Insta</a></li>
        <li class="active">@if(isset($id)) Изменение @else Добавление @endif аккаунта</li>
    </ol>
@endsection

@section('content')
    <!-- Отображение ошибок проверки ввода -->
    @include('common.errors')

    <form action="/accounts/@if(isset($id)){{$id}}@endif" method="POST" class="form-horizontal">

        {{ csrf_field() }}
        <div class="box-body">
            <div class="form-group">
                <label for="account-username" class="col-sm-2 control-label">Login</label>

                <div class="col-sm-4">
                    <input type="text" name="username" class="form-control" id="account-username" placeholder="@if(isset($id)){{$profile->username}} @else Login @endif " @if(isset($id)) disabled @endif>
                </div>
            </div>
            <div class="form-group">
                <label for="account-password" class="col-sm-2 control-label">Password</label>

                <div class="col-sm-4">
                    <input type="password" name="password" class="form-control" id="account-password" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <label for="account-proxy" class="col-sm-2 control-label">Proxy</label>
                <div class="col-sm-4">
                    @if(isset($proxy) && $proxy->count() != 0)
                        <select class="form-control" name="proxy" id="proxy">
                            @foreach($proxy as $item)
                                <option value="{{$item->id}}">{{$item->address}}:{{$item->port}}</option>
                            @endforeach
                        </select>
                    @else
                        Вам необходимо добавить прокси!
                    @endif
                </div>
            </div>
            @if(isset($proxy) && $proxy->count() != 0)
            <div class="form-group">
                <div class="col-sm-6">
                    <button type="button" class="btn btn-info pull-right" onclick="addAjax()" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i>@if(isset($id)) Изменить @else Добавить @endif аккаунт</button>
                </div>
            </div>
            @endif
        </div>
    </form>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Проверяем...</h4>
                </div>
                <div id="modal-body">

                </div>
                <div id="modal-footer">

                </div>
            </div>
        </div>
    </div>
@endsection


@section('head')
    <script>
        function addAjax() {
            $.ajax({
                type: 'POST',
                url: '',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data: {
                    username: $("[name = username]").val(),
                    password: $("[name = password]").val(),
                    proxy:    $("[name = proxy]").val()
                },
                success: function(data)
                {
                    switch(data['status'])
                    {
                        case 'exist':
                            $("#myModalLabel").html("Ошибка");
                            $("#modal-body").html('Такой аккаунт уже существует');
                            $("#modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
                            break;
                        case 'success':
                            $("#myModalLabel").html("Успешно");
                            $("#modal-body").html('Настройки успешно изменены' +
                                    '<a href="/">' +
                                    '<button type="button" class="btn btn-primary">Готово</button>' +
                                    '</a>');
                            break;
                        case 'confirm':
                            $("#myModalLabel").html("Ошибка");
                            $("#modal-body").html('<div style="margin: 0 auto; margin-top:10px; margin-bottom: 10px;">Подтвердите свой аккаунт</div>');
                            $("#modal-footer").html('<a href="'+data['message']+'" target="_blank">' +
                                    '<button type="button" class="btn btn-default">Подвердить</button>' +
                                    '</a>' +
                                    '<button type="button" class="btn btn-default" onclick="addAjax()">Я подтвердил</button>');
                            break;
                        case 'login':
                            $("#myModalLabel").html("Ошибка");
                            $("#modal-body").html(data['message']);
                            $("#modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
                            break;
                    }
                },
            });
        }
    </script>
@endsection