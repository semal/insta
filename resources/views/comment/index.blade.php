@extends('layouts.app')

@section('title')
Главная
@endsection

@section('content-header')
    <style type="text/css">
    </style><h1>&nbsp;</h1>
    <ol class="breadcrumb">
        <li><a href="/">Insta</a></li>
        <li class="active">Комментарии</li>
    </ol>
@endsection


@section('content')
<!-- Отображение ошибок проверки ввода -->
@include('common.errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Комментарии</h3>
                    <div class="tools pull-right">
                        <a href="<?echo URL::to('comments/create');?>"><button class="btn btn-success btn-md">Добавить новый</button></a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-condensed">
                        <tr>
                            <th style="width: 3% ">#</th>
                            <th style="width: 77%">Task</th>
                            <th style="width: 20%"></th>
                        </tr>
                        <?$i=1;?>
                        @foreach ($comments as $comment)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$comment->comment}}</td>
                                <td>
                                    <a href="<?echo URL::to('comments/edit/'.$comment->id);?>">
                                        <div class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></div>
                                    </a>
                                    <a href="<?echo URL::to('comments/delete/'.$comment->id);?>">
                                        <div class="btn btn-sm btn-danger"><i class="fa fa-close"></i></div>
                                    </a>
                                </td>
                            </tr>
                            <?$i++;?>
                        @endforeach
                    </table>
                    <div class="pagination pagination-sm no-margin pull-right">
                        {{ $comments->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('head')
@endsection