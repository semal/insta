@extends('layouts.app')

@section('title')
    Главная
@endsection

@section('content-header')
    <style type="text/css">
    </style><h1>&nbsp;</h1>
    <ol class="breadcrumb">
        <li><a href="/">Insta</a></li>
        <li class="active">Комментарии</li>
    </ol>
@endsection


@section('content')
    <!-- Отображение ошибок проверки ввода -->
    @include('common.errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">@if(isset($id))Редактировать @else Создать новый @endif комментарий</h3>
                    <div class="tools pull-right">
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="">
                        {{ csrf_field() }}
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" class="form-control" name="comment" placeholder="Комментарий" @if(isset($id))value="{{$comment->comment}}"@endif>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="submit" @if(isset($id)) value="Обновить"@else value="Добавить"@endif class="form-control pull-right">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('head')
@endsection