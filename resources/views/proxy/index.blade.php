@extends('layouts.app')

@section('title')
    Главная
@endsection

@section('content-header')
    <style type="text/css">
    </style><h1>&nbsp;</h1>
    <ol class="breadcrumb">
        <li><a href="/">Insta</a></li>
        <li class="active">Прокси</li>
    </ol>
@endsection


@section('content')
    <!-- Отображение ошибок проверки ввода -->
    @include('common.errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Список прокси</h3>
                    <div class="tools pull-right">
                        <a href="<?echo URL::to('proxy/create');?>"><button class="btn btn-success btn-md">Добавить новый прокси</button></a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-condensed">
                        <tr>
                            <th style="width: 3% ">#</th>
                            <th style="width: 15%">Тип</th>
                            <th style="width: 30%;">IP</th>
                            <th style="width: 15%;">login</th>
                            <th style="width: 17%">password</th>
                            <th style="width: 20%"></th>
                        </tr>
                        <?$i=1;?>
                        @foreach ($proxy as $item)
                            <tr>
                                <td>{{$i}}</td>
                                <td>
                                    http<?if($item->type == 1):?>s<?endif;?>
                                </td>
                                <td>
                                    {{$item->address}}:{{$item->port}}
                                </td>
                                <td>
                                    @if($item->login != NULL){{$item->login}} @else - @endif
                                </td>
                                <td>
                                    @if($item->password != NULL){{$item->password}} @else - @endif
                                </td>
                                <td>
                                    <a href="<?echo URL::to('proxy/edit/'.$item->id);?>">
                                        <div class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></div>
                                    </a>
                                    <a href="<?echo URL::to('proxy/delete/'.$item->id);?>">
                                        <div class="btn btn-sm btn-danger"><i class="fa fa-close"></i></div>
                                    </a>
                                </td>
                            </tr>
                            <?$i++;?>
                        @endforeach
                    </table>
                    <div class="pagination pagination-sm no-margin pull-right">
                        {{ $proxy->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('head')
@endsection