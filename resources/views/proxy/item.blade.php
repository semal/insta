@extends('layouts.app')

@section('title')
    Главная
@endsection

@section('content-header')
    <style type="text/css">
    </style><h1>&nbsp;</h1>
    <ol class="breadcrumb">
        <li><a href="/">Insta</a></li>
        <li class="active">Прокси</li>
    </ol>
@endsection


@section('content')
    <!-- Отображение ошибок проверки ввода -->
    @include('common.errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">@if(isset($id))Редактировать @else Добавить новый @endif прокси</h3>
                    <div class="tools pull-right">
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Type</label>
                            <div class="col-md-3">
                                <select class="form-control" name="type" id="type">
                                    <option value="0" @if(isset($proxy->type) && $proxy->type == 0) selected @endif>http</option>
                                    <option value="1" @if(isset($proxy->type) && $proxy->type == 1) selected @endif>https</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address">IP</label>
                            <div class="col-md-3">
                                <input class="form-control" type="text" id="address" name="address" placeholder="000.000.000.000"  @if(isset($proxy->address) &&  !empty($proxy->address)) value="{{$proxy->address}}" @endif>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="port">Port</label>
                            <div class="col-md-3">
                                <input class="form-control" type="text" id="port" name="port" placeholder="00000" @if(isset($proxy->port) && !empty($proxy->port)) value="{{$proxy->port}}" @endif>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="login">Login</label>
                            <div class="col-md-3">
                                <input class="form-control" type="text" id="login" name="login" @if(isset($proxy->login) && !empty($proxy->login)) value="{{$proxy->login}}" @endif>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <div class="col-md-3">
                                <input class="form-control" type="text" id="password" name="password" @if(isset($proxy->password) && !empty($proxy->password)) value="{{$proxy->password}}" @endif>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2">
                                <input class="form-control" type="submit" value="Добавить">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('head')
@endsection