@extends('layouts.app')

@section('content-header')
    <h1>
        {{$profile->username}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="/">Insta</a></li>
        <li class="active">{{$profile->username}}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 alerts">

        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{$profile->avatar}}" alt="User profile picture">

                    <h3 class="profile-username text-center">{{$profile->fullname}}</h3>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Подписчиков: </b> <a class="pull-right">{{$profile->follower}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Подписан на: </b> <a class="pull-right">{{$profile->following}}</a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?echo URL::to('accounts/profile/update/'.$profile->id);?>">
                                <div class="btn btn-danger btn-normal col-md-12">Обновить</div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tasks" data-toggle="tab">Задачи</a></li>
                </ul>
                <div class="tab-content">
                    <!-- tab -->
                    <div class="tab-pane active" id="tasks">
                        <div class="row">
                            <div class="col-md-8">
                                @foreach ($tasks as $task)
                                    <div class="row">
                                        <form method="post">
                                            {{ csrf_field() }}
                                            <div class="col-md-2">
                                                @if($task->type == 0)      {{--Подписка--}}
                                                <img src="http://s1.iconbird.com/ico/2013/6/269/w512h5121371227444friednadded.png" height="75px" alt="">
                                                @elseif($task->type == 1)  {{--Лайк--}}
                                                <img src="http://searchlikes.ru/blog/wp-content/uploads/2016/07/likevv.png" height="75px" alt="">
                                                @elseif($task->type == 2)  {{--Комментарий--}}
                                                <img src="http://s1.iconbird.com/ico/2013/9/450/w256h2561380453915Messaging256x25632.png" height="75px" alt="">
                                                @elseif($task->type == 3)  {{--Комментарий--}}
                                                <img src="http://a3.mzstatic.com/us/r30/Purple19/v4/5e/a9/63/5ea963fa-04d7-c9fa-bb43-e4297907f4aa/icon175x175.png" height="75px" alt="">
                                                @endif
                                            </div>
                                            <div class="col-md-8">
                                                <input name="count_range{{$task->id}}" type="text" onchange="changeFrequency({{$task->id}})" @if($task->active == 1) data-slider-enabled="false" @endif data-slider-value="{{$task->frequency}}" class="slider form-control" data-slider-min="1" data-slider-max="60" data-slider-step="1"  data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="blue">
                                            </div>
                                            <div class="col-md-2">
                                                <a href="<?echo URL::to('/task/'.$task->id.'/change_active');?>">
                                                    @if($task->active == 0)
                                                        <img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-play-128.png" width="40px" style="cursor: pointer;" alt="">
                                                    @elseif($task->active == 1)
                                                        <img src="http://iconizer.net/files/DefaultIcon_ver_0.11/orig/media-pause.png" width="40px" style="cursor: pointer;" alt="">
                                                    @endif
                                                </a>
                                            </div>
                                        </form>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-md-4">
                                @if(isset($proxy) && $proxy->count() > 0)
                                    <form action="" class="form-horizontal">
                                        <h4>Использовать прокси</h4>

                                        <div class="form-group">
                                            <input type="radio" name="proxy" onclick="changeProxy({{$profile->id}},0)" value="0" @if($profile->proxy == NULL) checked @endif> Ничего
                                        </div>

                                        @foreach($proxy as $item)
                                            <div class="form-group">
                                                <input type="radio" name="proxy" onclick="changeProxy({{$profile->id}},{{$item->id}})" value="{{$item->id}}" @if($profile->proxy_id == $item->id) checked @endif> {{$item->address}}:{{$item->port}}
                                            </div>
                                        @endforeach
                                    </form>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- endtab -->
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <h3>Targeting</h3>
                    <form action="javascript:void(null)" method="post" onsubmit="call()" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="col-md-3">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="type" value="0" @if($target->sex != NULL) checked @endif>
                                    По полу
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="type" value="1" @if($target->hashtag != NULL) checked @endif>
                                    По хештэгу
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="type" value="2" @if($target->geotag != NULL) checked @endif>
                                    По геотэгу
                                </label>
                            </div>
                        </div>
                        <div class="col-md-2 target-tab" id="sex-target">
                            <label for="sex">По полу</label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sex" value="0" onclick="type_target(0)" @if($target->sex != NULL) @if($target->sex == 0) checked @endif @else disabled @endif>
                                    Мужской
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sex" value="1" onclick="updateType(0)" @if($target->sex != NULL) @if($target->sex == 1) checked @endif @else disabled @endif>
                                    Женский
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3 target-tab" id="hashtag-target">
                            <label for="hashtag">По хештэгу</label>
                            <div class="input-group">
                                <span class="input-group-addon">#</span>
                                <input type="text" class="form-control" name="hashtag" onclick="updateType(1)" placeholder="Хештэг" @if($target->hashtag != NULL) value={{$target->hashtag}} @else disabled @endif>
                            </div>
                        </div>
                        <div class="col-md-3 target-tab" id="geotag-target">
                            <label for="geotag">По геотэгу</label>
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input type="text" class="form-control" name="geotag" onclick="updateType(2)" placeholder="Геотэг" @if($target->geotag != NULL) value={{$target->geotag}} @else disabled @endif>
                            </div>
                        </div>
                        <button class="btn pull-right" style="margin-right: 150px;" type="button" onclick="callTargeting()">
                            Обновить
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('head')

    <link rel="stylesheet" href="{{url('plugins/bootstrap-slider/slider.css')}}">
    <script type="text/javascript" src="{{url('plugins/bootstrap-slider/bootstrap-slider.js')}}"></script>

    <link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

    <script src="{{url('js/bootstrap-datetimepicker.min.js')}}"></script>


    <script type="text/javascript">
        var buttons, sex, hashtag, geotag;
        var type_target = 0;
        $(document).ready(function () {

            $('.slider').slider();

            buttons = document.getElementsByName("type");
            sex = document.getElementsByName("sex");
            hashtag = document.getElementsByName("hashtag");
            geotag = document.getElementsByName("geotag");
            for (var i = 0; i < buttons.length; i++)
            {
                buttons[i].addEventListener("change", changeTypeTarget);
            }
            function changeTypeTarget(event) {
                switch (event.target.value)
                {
                    case "0":
                        for(i=0; i < sex.length; i++)
                        {
                            sex[i].disabled = "";
                        }
                        hashtag[0].disabled = "disabled";
                        geotag[0].disabled = "disabled";
                        type_target = 0;
                        break;
                    case "1":
                        for(i=0; i < sex.length; i++)
                        {
                            sex[i].disabled = "disabled";
                        }
                        hashtag[0].disabled = "";
                        geotag[0].disabled = "disabled";
                        type_target = 1;
                        break;
                    case "2":
                        for(i=0; i < sex.length; i++)
                        {
                            sex[i].disabled = "disabled";
                        }
                        hashtag[0].disabled = "disabled";
                        geotag[0].disabled = "";
                        type_target = 2;
                        break;
                }
            }
        });

        function changeFrequency(id) {
            var frequency = $("[name = count_range"+id+"]").val();
            $.ajax({
                type: 'POST',
                url: '/task/'+id+'/change_frequency',
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data: {
                    frequency: frequency,

                },
            });
        }

        function changeProxy(profile_id, id) {
            var proxy = $("[name = proxy]").val();
            $.ajax({
                type: 'POST',
                url: '/proxy/change/'+profile_id+'/'+id,
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
            });
        }

        function updateType(type) {
            type_target = type;
        }

        function callTargeting() {
            var account_id = {{$profile->id}};
            switch(type_target)
            {
                case 0:
                    field = "sex";
                    for(i=0; i < sex.length; i++)
                    {
                        if(sex[i].checked)
                        {
                            value = sex[i].value;
                        }
                    }
                    break;
                case 1:
                    field = "hashtag";
                    value = hashtag[0].value;
                    break;
                case 2:
                    field = "geotag";
                    value = geotag[0].value;
                    break;
            }
            $.ajax({
                type: 'POST',
                url: '/accounts/profile/'+account_id+'/change_target',
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data: {
                    field: field,
                    value: value,
                },
                statusCode: {
                    200:function () {
                        $(".alerts").html("<div class='alert alert-success alert-dismissible'>" +
                                "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" +
                                "<h4><i class='icon fa fa-check'></i> Выполнено!</h4>" +
                                "Настройки поиска для этого аккаунта успешно обновлены." +
                                "</div>");
                    }
                }
            });
        }
    </script>
@endsection