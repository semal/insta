@extends('layouts.app')

@section('title')
    Главная
@endsection

@section('content-header')
    <style type="text/css">
    </style><h1>&nbsp;Статистика</h1>
    <ol class="breadcrumb">
        <li><a href="/">Insta</a></li>
        <li class="active">Статистика</li>
    </ol>



@endsection


@section('content')
    <!-- Отображение ошибок проверки ввода -->
    @include('common.errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-body">
                    <form action="javascript:void">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-4">
                                <select name="account" id="account" class="form-control" onchange="changeStat(); changeFollowers()">
                                    <option value="0" selected>Все аккаунты</option>
                                    @foreach($accounts as $account)
                                        <option value="{{$account->id}}">{{$account->username}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select name="type" id="type" class="form-control" onchange="changeStat()">
                                    <option value="0">Все</option>
                                    <option value="1">Лайки</option>
                                    <option value="2">Подписки</option>
                                    <option value="3">Комментарии</option>
                                    <option value="4">Отписки</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Действия</h3>
                </div>
                <div class="box-body chart-responsive">
                    <div class="chart" id="line-chart" style="height: 300px;"></div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Подписки</h3>
                </div>
                <div class="box-body chart-responsive">
                    <div class="chart" id="followers-chart" style="height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('head')
    <!-- Morris charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="{{url('plugins/morris/morris.min.js')}}"></script>
    <link rel="stylesheet" href="{{url('plugins/morris/morris.css')}}">

    <script>

        var stat;
        var followers;

        var info = [];
        var infoFollowers = [];

        function data()
        {
            var ret = [];
            @foreach($tasks as $task)
                 ret.push({day: '{{$task['time']}}', item1: {{$task['count_time']}}});
            @endforeach
            return ret;
        }

        function dataFollowers()
        {
            var ret = [];
            @foreach($followers as $follower)
                 ret.push({day: '{{$follower['time']}}', item1: {{$follower['count']}}});
            @endforeach
            return ret;
        }

        function changeStat() {
            stat.redraw();
            info = [];
            $.ajax({
                type: 'POST',
                url: '',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data: {
                    id: $("[name = account]").val(),
                    type: $("[name = type]").val()
                },
                success: function(list)
                {
                    list.forEach(function(entry){
                        info.push({day: entry.time, item1: entry.count_time});
                    });
                    stat.setData(info);
                },
            });
        }

        function changeFollowers() {
           // followers.redraw();
            infoFollowers = [];
            $.ajax({
                type: 'POST',
                url: 'stat_followers',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data: {
                    id: $("[name = account]").val(),
                },
                success: function(list)
                {
                    list.forEach(function(entry){
                        infoFollowers.push({day: entry.time, item1: entry.count});
                    });
                    followers.setData(infoFollowers);
                },
            });
        }




        $(function () {
            "use strict";

            stat = new Morris.Line({
                element: 'line-chart',
                resize: true,
                data: [],
                xLabels: 'day',
                xkey: 'day',
                ykeys: ['item1'],
                labels: ['Выполнено задач'],
                lineColors: ['#3c8dbc'],
                hideHover: 'auto'
            });
            stat.setData(data());


            followers = new Morris.Line({
                element: 'followers-chart',
                resize: true,
                data: [],
                xLabels: 'day',
                xkey: 'day',
                ykeys: ['item1'],
                labels: ['Изменение количества'],
                lineColors: ['#3c8dbc'],
                hideHover: 'auto'
            });
            followers.setData(dataFollowers());
        });


</script>
@endsection