@if (!empty($message))
    <div class="alert alert-danger">
        @if($code == 1)
            Логин обязателен
        @elseif($code == 2)
            feedback обязателен
        @elseif($code == 3)
            Необходимо подтвердить аккаунт
        @elseif($code == 4)
            Введен неправильный пароль
        @elseif($code == 5)
            Пустая ошибка
        @elseif($code == 6)
            Аккаунт забанен
        @elseif($code == 7)
            sentry_block
        @elseif($code == 8)
            Некорректный пользователь
        @elseif($code == 9)
            Пароль сброшен
        @else
            Непредвиденная ошибка. Возможно, необходимо проверить аккаунт
        @endif
    </div>
    <a href="">Обновить</a>
@endif