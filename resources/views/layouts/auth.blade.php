<html>
<head>
    <link rel="stylesheet" href="{{url('css/app.css')}}">
    <link rel="stylesheet" href="{{url('css/style.css')}}">
    <title>Insta - @yield('title')</title>
</head>
<body>
@yield('header')
<div class="container">
    <div class="row portlet-body">
        <div class="col-xs-12">
            @yield('content')
        </div>
    </div>

</div>
</body>
</html>