@extends('layouts.app')

@section('title')
    Главная
@endsection

@section('profile')
    Нет информации о пользователе
@endsection

@section('content-header')
    <h1>
        Мои аккаунты
    </h1>
    <ol class="breadcrumb">
        <li><a href="/">Insta</a></li>
        <li class="active">Мои аккаунты</li>
    </ol>
@endsection

@section('content')
    <? $i = 1;?>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <table class="table table-condensed">
                    <tr>
                        <th style="width: 10px">#</th>
                        <th width="40%">Account</th>
                        <th>Tools</th>
                        <th></th>
                    </tr>
                    @if (count($accounts) > 0)

                        @foreach ($accounts as $account)
                            <tr>
                                <td>{{$i}}</td>
                                <td class="table-text">
                                    <div><a href="<?echo URL::to('accounts/profile/'.$account->id);?>">{{ $account->username }}</a></div>
                                </td>
                                <td>
                                    <a href="<?echo URL::to('accounts/'.$account->id);?>">
                                        <div class="btn btn-sm btn-warning">Изменить</div>
                                    </a>
                                    <a href="<?echo URL::to('accounts/delete/'.$account->id);?>" onclick="return confirmDelete();">
                                        <div class="btn btn-sm btn-danger">Отвязать</div>
                                    </a>
                                </td>
                                <td>
                                    @if($account->warning == 1)
                                        <i class="glyphicon glyphicon-exclamation-sign"></i> Требуется подтверждение аккаунта!
                                    @elseif($account->warning == 2)
                                        <i class="glyphicon glyphicon-exclamation-sign"></i> Аккаунт забанен!
                                    @endif
                                </td>
                            </tr>
                            <?$i++;?>
                        @endforeach
                    @endif
                </table>
            </div>
        </div>
    </div>
    <a href="<?echo URL::to('accounts');?>">Добавить аккаунт</a>

@endsection

@section('head')
    <script>
        function confirmDelete() {
            if (confirm("Вы подтверждаете удаление?")) {
                return true;
            } else {
                return false;
            }
        }
    </script>
    @endsection