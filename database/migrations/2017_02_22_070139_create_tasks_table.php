<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->index();
            $table->integer('type')->comment('Тип задачи');
            $table->double('frequency')->comment('Частота');
            $table->integer('active')->comment('0 выкл, 1 вкл');
            $table->integer('count')->nullable()->comment('Количество задач');
            $table->dateTime('time_begin')->nullable()->comment('Время начала выполнения');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
