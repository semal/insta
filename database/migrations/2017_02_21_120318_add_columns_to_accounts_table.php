<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function ($table){
            $table->bigInteger('instagram_id');
            $table->string('fullname')->nullable();
            $table->string('avatar',255);
            $table->integer('following');
            $table->integer('follower');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function ($table){
            $table->dropColumn('instagram_id');
            $table->dropColumn('fullname');
            $table->dropColumn('avatar');
            $table->dropColumn('following');
            $table->dropColumn('follower');
        });
    }
}
