<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCompletedTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('completed_tasks', function ($table)
       {
           $table->integer('user_id')->nullable();
           $table->integer('media_id')->nullable();
           $table->text('comment')->nullable();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('completed_tasks', function ($table) {
            $table->dropColumn('user_id');
            $table->dropColumn('media_id');
            $table->dropColumn('comment');
        });
    }
}
