<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PanelController@index');

Route::get('/accounts', 'PanelController@accounts');
Route::post('/accounts', 'PanelController@add_account');

Route::get('/accounts/{id}', 'PanelController@edit_account');
Route::post('/accounts/{id}', 'PanelController@update_account');

Route::get('/accounts/delete/{id}', 'PanelController@delete_account');

Route::get('/accounts/profile/{id}', 'PanelController@profile');
Route::get('/accounts/profile/update/{id}', 'PanelController@update');


Route::post('/task/create','TaskController@create');
Route::delete('/task/delete/{task}', 'TaskController@destroy');

Route::post('/task/{task}/change_frequency', 'TaskController@changeFrequency');
Route::get('/task/{task}/change_active', 'TaskController@changeActive');

Route::post('/accounts/profile/{account_id}/change_target', 'PanelController@change_target');

//comment
Route::get('comments', 'CommentController@index');
Route::get('comments/delete/{id}', 'CommentController@delete');
Route::match(['get','post'],'/comments/create','CommentController@create');
Route::match(['get','post'],'/comments/edit/{id}','CommentController@edit');

//proxy
Route::get('proxy', 'ProxyController@index');
Route::get('proxy/delete/{id}', 'ProxyController@delete');
Route::post('proxy/change/{profile_id}/{id}', 'ProxyController@change');
Route::match(['get','post'],'/proxy/create','ProxyController@create');
Route::match(['get','post'],'/proxy/edit/{id}','ProxyController@edit');

//статистика
Route::get('statistic', 'StatisticController@index');
Route::post('statistic', 'StatisticController@get_statistic');
Route::post('stat_followers', 'StatisticController@get_followers');


Route::get('profile/test','PanelController@test');

Route::get('/faq',function ()
{
    return view('faq');
});

Auth::routes();

Route::get('/auth', [
    'uses'=>'AuthController@index'
]);



