<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public $fillable = [
        'type',
        'frequency',
        'count',
        'active',
        'time_begin',
    ];

    protected $dates = ['created_at', 'updated_at', 'time_begin'];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
