<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proxy extends Model
{
    protected $table = 'proxy';

    protected $fillable = [
        'type',
        'address',
        'port',
        'login',
        'password',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
