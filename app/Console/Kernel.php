<?php

namespace App\Console;

use App\Follower;
use App\Proxy;
use App\Task;
use App\Target;
use App\Account;
use InstagramAPI\Instagram;
use InstagramAPI\InstagramException;
use App\Tasks\Search\SearchForTask;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function (){
            $time = date("Y-m-d H:i:00",time());
            $tasks = Task::where('time_begin','<=',$time)
                ->where('active',1)
                ->get();
            $instagram = new Instagram();
            if($tasks->count() > 0) {
                foreach ($tasks as $task) {

                    //Сначала обновим время, чтобы работа скрипта не прекратилась
                    $frequency = $task->frequency;
                    $time_begin = $task->time_begin;
                    //Интервал между 2 задачами в секундах
                    $delta = 3600 / $frequency;
                    //TODO: Костыль. Пофиксить
                    $task->time_begin = date("Y-m-d H:i:00", strtotime($time) + $delta + 59);

                    if ($task->count != NULL) {
                        $task->count--;
                    }
                    $task->save();
                    $profile = Account::find($task->account_id);
                    try
                    {
                        //Выполняем задачу
                        $target  = Target::where('account_id',$task->account_id)->take(1)->get();

                        //Логинимся
                        if($profile->proxy != NULL)
                        {
                            $proxy = Proxy::find($profile->proxy);
                            $link = "";
                            switch ($proxy->type) {
                                case 0:
                                    $link .= "http://";
                                    break;
                                case 1:
                                    $link .= "https://";
                                    break;
                            }
                            if (isset($proxy->address) && !empty($proxy->address)) {
                                $login = NULL;
                                $password = NULL;

                                if (!empty($proxy->login) && !empty($proxy->password)) {
                                    $login = $proxy->login;
                                    $password = $proxy->password;

                                    $link .= $proxy->login . ":" . $proxy->password . "@";
                                }

                                $link .= $proxy->address . ":" . $proxy->port;

                                $ch = curl_init("http://insta.at-develop.ru");
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_PROXY, $link);
                                $response = curl_exec($ch);
                                curl_close($ch);
                                if (!empty($response)) {
                                    $instagram->setProxy($link);
                                }
                            }
                        }

                        $instagram->setUser($profile->username, decrypt($profile->password));
                        $instagram->login();

                        $search = new SearchForTask($instagram,$target[0]);

                        $obj = $search->getTask()->make($task->type,$task->account_id);
                        $obj->execute();

                        $profile->warning = 0;
                        $profile->save();

                        unset($search);
                        unset($obj);
                    } catch(InstagramException $e)
                    {
                        $code = $e->getCode();
                        if($code == 1 || $code == 3)
                        {
                            $profile->warning = 1;
                        }
                        elseif($code == 6)
                        {
                            $profile->warning = 2;
                        }
                        $profile->warning = 1;
                        $profile->save();
                        continue;
                    }
                }
            }
        })->everyMinute();

        $schedule->call(function (){
            $instagram = new \InstagramAPI\Instagram();
            $profiles = Account::all();
            foreach ($profiles as $profile)
            {
                $instagram->setUser($profile->username, decrypt($profile->password));
                $instagram->login();
                //Получаем текущий профиль
                $instagram_user = $instagram->getSelfUserInfo();
                //Обновляем информацию
                $profile->fullname = $instagram_user->user->full_name;
                $profile->avatar = $instagram_user->user->profile_pic_url;
                $profile->following = $instagram_user->user->following_count;
                $profile->follower = $instagram_user->user->follower_count;

                $profile->save();

                $followers = new Follower([
                    'followers' => $profile->follower
                ]);

                $profile->followers()->save($followers);
            }
            sleep(3);
        })->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
