<?php

namespace App\Http\Controllers;


use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $comments = Comment::paginate(50);

        return view('comment.index',[
            'comments' => $comments
        ]);
    }

    public function create(Request $request)
    {
        if(isset($request->comment) && !empty($request->comment))
        {
            $comment = new Comment([
                'comment' => $request->comment
            ]);
            $comment->save();

            return redirect('/comments');
        }
        return view('comment.item');
    }

    public function edit(Request $request, $id)
    {
        $comment = Comment::find($id);
        if(isset($request->comment) && !empty($request->comment))
        {
            $comment->comment = $request->comment;
            $comment->save();

            return redirect('/comments');
        }

        return view('comment.item',[
            'id' => $id,
            'comment' => $comment
        ]);
    }

    public function delete(Request $request, $id)
    {
        Comment::destroy($id);
        return redirect('/comments');
    }
}
