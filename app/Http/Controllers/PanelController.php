<?php

namespace App\Http\Controllers;

use App\Follower;
use InstagramAPI\Instagram;

use App\Target;
use App\Task;
use App\Account;
use App\Proxy;

use App\Tasks\Search\SearchForTask;
use App\CompletedTask;

use Illuminate\Http\Request;
use InstagramAPI\InstagramException;
use League\Flysystem\Exception;

class PanelController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /* Главная панели */
    public function index(Request $request)
    {
        $accounts = Account::where('user_id', $request->user()->id)->get();
        return view('insta', [
            'title' => 'Мои аккаунты',
            'accounts' => $accounts,
        ]);
    }

    /*Страница добавления аккаунта*/
    public function accounts(Request $request)
    {
        $proxy = Proxy::all();
        return view('account',[
            'proxy' => $proxy
        ]);
    }

    //Профиль аккаунта
    public function profile(Request $request, $id)
    {
        $profile = Account::find($id);
        $target = Target::where('account_id',$id)->first();
        $proxy = Proxy::where('user_id',$request->user()->id)->get();
        $default_tasks = $profile->tasks->where('account_id',$id)->where('count',0);

        return view('profile.index',[
            'id' => $id,
            'profile' => $profile,
            'tasks' => $default_tasks,
            'target' => $target,
            'proxy'  => $proxy,
        ]);
    }

    public function test()
    {
        $time = date("Y-m-d H:i:00",time());
        $tasks = Task::where('time_begin','<=',$time)
            ->where('active',1)
            ->get();
        $instagram = new Instagram();
        if($tasks->count() > 0) {
            foreach ($tasks as $task) {

                //Сначала обновим время, чтобы работа скрипта не прекратилась
                $frequency = $task->frequency;
                $time_begin = $task->time_begin;
                //Интервал между 2 задачами в секундах
                $delta = 3600 / $frequency;
                //TODO: Костыль. Пофиксить
             //   $task->time_begin = date("Y-m-d H:i:00", strtotime($time) + $delta + 59);

                if ($task->count != NULL) {
                    $task->count--;
                }
                $task->save();
                $profile = Account::find($task->account_id);
               /* try
                {*/
                    //Выполняем задачу
                    $target  = Target::where('account_id',$task->account_id)->take(1)->get();

                    //Логинимся
                    if($profile->proxy != NULL)
                    {
                        $proxy = Proxy::find($profile->proxy);
                        $link = "";
                        switch ($proxy->type) {
                            case 0:
                                $link .= "http://";
                                break;
                            case 1:
                                $link .= "https://";
                                break;
                        }
                        if (isset($proxy->address) && !empty($proxy->address)) {
                            $login = NULL;
                            $password = NULL;

                            if (!empty($proxy->login) && !empty($proxy->password)) {
                                $login = $proxy->login;
                                $password = $proxy->password;

                                $link .= $proxy->login . ":" . $proxy->password . "@";
                            }

                            $link .= $proxy->address . ":" . $proxy->port;

                            $ch = curl_init("http://insta.at-develop.ru");
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_PROXY, $link);
                            $response = curl_exec($ch);
                            curl_close($ch);
                            if (!empty($response)) {
                                $instagram->setProxy($link);
                            }
                        }
                    }

                    $instagram->setUser($profile->username, decrypt($profile->password));
                    $instagram->login();

                    $search = new SearchForTask($instagram,$target[0]);

                    $obj = $search->getTask()->make($task->type,$task->account_id);
                    $obj->execute();

                    $profile->warning = 0;
                    $profile->save();

                    unset($search);
                    unset($obj);
                /*} /*catch(InstagramException $e)
                {
                    $code = $e->getCode();
                    if($code == 1 || $code == 3)
                    {
                        $profile->warning = 1;
                    }
                    elseif($code == 6)
                    {
                        $profile->warning = 2;
                    }
                    $profile->warning = 1;
                    $profile->save();
                    continue;
                }*/
            }
        }
    }

    //Добавление аккаунта ajax
    public function add_account(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|max:255',
            'password' => 'required|max:255',
            'proxy'    => 'required'
        ]);
        //Есть ли такой пользователь в базе
        $list = Account::where('username',$request->username)->first();
        if(!empty($list))
        {
            echo json_encode(array('status'=>'exist'));;
            exit;
        }

        //Проверяем, существует ли такой пользователь
        try
        {
            $instagram = new Instagram();

            $proxy = Proxy::find((int)$request->proxy);

            $link = "";
            switch ($proxy->type) {
                case 0:
                    $link .= "http://";
                    break;
                case 1:
                    $link .= "https://";
                    break;
            }
            if (isset($proxy->address) && !empty($proxy->address)) {
                $login = NULL;
                $password = NULL;

                if (!empty($proxy->login) && !empty($proxy->password)) {
                    $login = $proxy->login;
                    $password = $proxy->password;

                    $link .= $proxy->login . ":" . $proxy->password . "@";
                }

                $link .= $proxy->address . ":" . $proxy->port;

                $ch = curl_init("http://insta.at-develop.ru");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXY, $link);
                $response = curl_exec($ch);
                curl_close($ch);
                if (!empty($response)) {
                    $instagram->setProxy($link);
                }
            }

            $instagram->setUser($request->username, $request->password);
            $instagram->login();
        }
        catch (InstagramException $e)
        {
            $code = $e->getCode();
            $message = $e->getMessage();
            switch ($code)
            {
                case 3:
                    echo json_encode(array('status'=>'confirm','message'=>$message,'code'=>$code));
                    break;
                case 4:
                    $message = "Некорректный логин или пароль";
                    echo json_encode(array('status'=>'login','message'=>$message,'code'=>$code));
                    break;
            }

            exit;
        }

        $instagram_id = $instagram->getUsernameId($request->username);
        $instagram_user = $instagram->getSelfUserInfo();

        //Создаем новый аккаунт
        $account = $request->user()->accounts()->create([
            'username'     => $request->username,
            'password'     => encrypt($request->password),
            'instagram_id' => $instagram_id,
            'fullname'     => $instagram_user->user->full_name,
            'avatar'       => $instagram_user->user->profile_pic_url,
            'following'    => $instagram_user->user->following_count,
            'follower'     => $instagram_user->user->follower_count,
            'proxy_id'     => (int)$request->proxy,
        ]);


        //Создаем задачи всех типов
        $like = new Task([
            'type' => 1,
            'frequency' => 5,
            'active' => 0
        ]);
        $account->tasks()->save($like);

        $follow = new Task([
            'type' => 0,
            'frequency' => 5,
            'active' => 0
        ]);
        $account->tasks()->save($follow);

        $comment = new Task([
            'type' => 2,
            'frequency' => 5,
            'active' => 0
        ]);
        $account->tasks()->save($comment);

        $unfollow = new Task([
            'type' => 3,
            'frequency' => 5,
            'active' => 0
        ]);
        $account->tasks()->save($unfollow);

        $target = new Target([]);
        $account->targets()->save($target);

        echo json_encode(array('status'=>'success'));
        exit;
    }

    public function change_target(Request $request, $id)
    {
        $target = Target::where('account_id',$id)->first();
        $target->sex = NULL;
        $target->hashtag = NULL;
        $target->geotag = NULL;
        $field = $request->field;
        $target->$field = $request->value;

        $target->save();
    }

    public function delete_account(Request $request,$id)
    {
        Task::where('account_id',$id)->delete();
        Target::where('account_id',$id)->delete();
        Account::destroy($id);
        return redirect('/');
    }

    public function update(Request $request, $id)
    {
        $instagram = new \InstagramAPI\Instagram();
        $profile = Account::find($id);

        $instagram->setUser($profile->username, decrypt($profile->password));
        $instagram->login();
        //Получаем текущий профиль
        $instagram_user = $instagram->getSelfUserInfo();
        //Обновляем информацию
        $profile->fullname = $instagram_user->user->full_name;
        $profile->avatar = $instagram_user->user->profile_pic_url;
        $profile->following = $instagram_user->user->following_count;
        $profile->follower = $instagram_user->user->follower_count;

        $profile->save();

        return redirect('/accounts/profile/'.$id);
    }

    public function edit_account(Request $request, $id)
    {
        $proxy = Proxy::all();
        $profile = Account::find($id);
        return view('account',[
            'id'=> $id,
            'profile' => $profile,
            'proxy' => $proxy
        ]);
    }

    public function update_account(Request $request, $id)
    {

        $this->validate($request, [
            'password' => 'required|max:255'
        ]);

         $profile = Account::find($id);
        //Проверяем, существует ли такой пользователь
        try
        {
            $instagram = new Instagram();
          //  $instagram->setProxy(getenv('APP_PROXY'));
            $instagram->setUser($profile->username, $request->password);
            $instagram->login();
        }
        catch (InstagramException $e)
        {

            $code = $e->getCode();
            $message = $e->getMessage();
            switch ($code)
            {
                case 3:
                    $message = explode(':',$message);
                    $message = $message[0].":/".$message[1];
                    echo json_encode(array('status'=>'confirm','message'=>$message,'code'=>$code));
                    break;
                case 4:
                    $message = "Некорректный логин или пароль";
                    echo json_encode(array('status'=>'login','message'=>$message,'code'=>$code));
                    break;
            }

            exit;
        }

        $profile->password = encrypt($request->password);

        $profile->save();
        echo json_encode(array('status'=>'success'));
        exit;
    }

}