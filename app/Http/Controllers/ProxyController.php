<?php

namespace App\Http\Controllers;

use App\Account;
use App\Proxy;
use Illuminate\Http\Request;

class ProxyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $proxy = Proxy::paginate(50);

        return view('proxy.index',[
            'proxy' => $proxy
        ]);
    }

    public function create(Request $request)
    {
        if(isset($request->address) && !empty($request->address))
        {
            /*
             * TODO: валидация
             */
            $proxy = "";
            switch ($request->type)
            {
                case 0:
                    $proxy .= "http://";
                    break;
                case 1:
                    $proxy .= "https://";
                    break;
            }

            $login = NULL;
            $password = NULL;
            if(!empty($request->login) && !empty($request->password))
            {
                $login = $request->login;
                $password = $request->password;

                $proxy .= $request->login.":".$request->password."@";
            }

            $proxy .= $request->address.":".$request->port;

            $ch = curl_init("http://insta.at-develop.ru");
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch,CURLOPT_PROXY,$proxy);
            $response = curl_exec($ch);
            curl_close($ch);

            if(!empty($response))
            {
                $request->user()->proxy()->create([
                    'type'     => $request->type,
                    'address'  => $request->address,
                    'port'     => $request->port,
                    'login'    => $login,
                    'password' => $password,
                ]);

                return redirect('/proxy');
            }
        }
        return view('proxy.item');
    }

    public function edit(Request $request, $id)
    {
        $proxy = Proxy::find($id);

        $link = "";
        switch ($request->type)
        {
            case 0:
                $link .= "http://";
                break;
            case 1:
                $link .= "https://";
                break;
        }
        if(isset($request->address) && !empty($request->address))
        {
            $login = NULL;
            $password = NULL;

            if(!empty($request->login) && !empty($request->password))
            {
                $login = $request->login;
                $password = $request->password;

                $link .= $request->login.":".$request->password."@";
            }

            $link .= $request->address.":".$request->port;

            $ch = curl_init("http://insta.at-develop.ru");
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch,CURLOPT_PROXY,$link);
            $response = curl_exec($ch);
            curl_close($ch);
            if(!empty($response))
            {
                $proxy->type      = $request->type;
                $proxy->address   = $request->address;
                $proxy->port      = $request->port;
                $proxy->login     = $login;
                $proxy->password  = $password;
                $proxy->save();
            }
            return redirect('/proxy');
        }

        return view('proxy.item',[
            'id' => $id,
            'proxy' => $proxy
        ]);
    }

    public function delete(Request $request, $id)
    {
        Proxy::destroy($id);
        return redirect('/proxy');
    }

    public function change(Request $request, $profile_id, $id)
    {
        $profile = Account::find($profile_id);
        if($id == 0)
        {
            $profile->proxy_id = NULL;
        }
        else
        {
            $profile->proxy_id = $id;
        }
        $profile->save();
    }
}
