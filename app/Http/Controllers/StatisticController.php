<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Account;
use App\Follower;
use App\CompletedTask;
use Illuminate\Http\Request;

class StatisticController extends Controller
{
    public function index(Request $request)
    {
        $time = date("Y-m-d H:i:00",time());
        //Получить список аккаунтов пользователя
        $accounts = Account::where('user_id', $request->user()->id)->get();
        $ids = array();
        foreach ($accounts as $account)
        {
            $ids[] = $account->id;
        }
        //Получить список выполненных задач за этот месяц
        //Отсортировать по дням
        $completed_tasks = CompletedTask::select(DB::raw('date(time) as time, count(time) as count_time, type, account_id'))
            ->whereIn('account_id',$ids)
            ->whereMonth('time',Carbon::now()->month)
            ->groupBy(DB::raw('DATE(time)'))
            ->get()->toArray();

        //Динамика подписчиков
        $followers = Follower::select(DB::raw('date(created_at) as time, followers'))
            ->whereIn('account_id',$ids)
            ->whereMonth('created_at',Carbon::now()->month);
        $followers = $followers->get()->toArray();
        $out = [];
        foreach ($followers as $key => $item) {
            if($key == 0)
            {
                $out[] = [
                    'time'=>$item['time'],
                    'count'=>0
                ];
                continue;
            }
            $out[] = [
                'time' => $item['time'],
                'count' => $followers[$key]['followers']-$followers[$key-1]['followers']
            ];
        }

        //Передать во вьюху
        return view('statistic.index',[
            'accounts' => $accounts,
            'tasks' => $completed_tasks,
            'followers' => $out
        ]);
    }

    public function get_statistic(Request $request)
    {
        //Получить список аккаунтов пользователя
        $accounts = Account::where('user_id', $request->user()->id)->get();
        $ids = array();
        foreach ($accounts as $account)
        {
            $ids[] = $account->id;
        }
        switch ($request->type)
        {
            case "0":
                $type = NULL;
                break;
            case "1":
                $type = 1;
                break;
            case "2":
                $type = 0;
                break;
            case "3":
                $type = 2;
                break;
            case "4":
                $type = 3;
                break;
        }
        $tasks = CompletedTask::select(DB::raw('date(time) as time, count(time) as count_time, type'))
            ->whereMonth('time',Carbon::now()->month)
            ->groupBy(DB::raw('DATE(time)'));
        //Получаем, если не все аккаунты
        if($request->id != "0")
        {
            $tasks->where('account_id',$request->id);
        }
        else
        {
            //Если все, то получаем по аккаунтам текущего пользователя
            $tasks->whereIn('account_id',$ids);
        }
        if($type != NULL)
        {
            $tasks->where('type',$type);
        }
        $tasks = $tasks->get()->toArray();
        echo json_encode($tasks);
    }

    public function get_followers(Request $request)
    {
        //Получить список аккаунтов пользователя
        $accounts = Account::where('user_id', $request->user()->id)->get();
        $ids = array();
        foreach ($accounts as $account)
        {
            $ids[] = $account->id;
        }
        $followers = Follower::select(DB::raw('date(created_at) as time, followers'))
            ->whereMonth('created_at',Carbon::now()->month);
        //Получаем, если не все аккаунты
        if($request->id != "0")
        {
            $followers->where('account_id',$request->id);
        }
        else
        {
            //Если все, то получаем по аккаунтам текущего пользователя
            $followers->whereIn('account_id',$ids);
        }
        $followers = $followers->get()->toArray();
        $out = [];
        foreach ($followers as $key => $item) {
            if($key == 0)
            {
                $out[] = [
                    'time'=>$item['time'],
                    'count'=>0
                ];
                continue;
            }
            $out[] = [
                'time' => $item['time'],
                'count' => $followers[$key]['followers']-$followers[$key-1]['followers']
                ];
       }


        echo json_encode($out);
    }
}
