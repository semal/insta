<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    public $fillable = [
        'followers'
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
