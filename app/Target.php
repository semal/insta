<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    protected $fillable = [
        'hashtag',
        'geotag',
        'sex'
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
