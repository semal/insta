<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompletedTask extends Model
{
    //Имя таблицы
    protected $table = 'completed_tasks';

    public $fillable = [
        'type',
        'time',
        'user_id',
        'media_id',
        'comment'
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

}
