<?php

namespace App\Tasks\Search;

use App\Tasks\TaskFactory;

class SearchForTask
{
    private $instagram;
    private $target;

    private $search;

    public function __construct(\InstagramAPI\Instagram $instagram, $target)
    {
        $this->instagram = $instagram;
        $this->target    = $target;
        //Анализируем таргет
        $search = $this->analysis();
        $search->search();
        $this->search = $search;
    }

    public function getTask()
    {
        return new TaskFactory($this->instagram, $this->search->user_id, $this->search->media_id);
    }

    private function analysis()
    {

        if($this->target->hashtag !== NULL)
        {
            return new HashtagSearch($this->instagram,$this->target->hashtag);
        }
        elseif($this->target->geotag !== NULL)
        {
            return new GeotagSearch($this->instagram,$this->target->geotag);
        }
        elseif($this->target->sex !== NULL)
        {
            return new SexSearch($this->instagram,$this->target->sex);
        }
        else
        {
            //По дефолту ищем мужчин
            $this->target->sex = 0;
            return new SexSearch($this->instagram,$this->target->sex);
        }
    }
}