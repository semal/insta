<?php

namespace App\Tasks\Search;

class GeotagSearch
{
    public $user_id = 0;
    public $media_id = 0;

    private $instagram;
    private $tag;

    public function __construct(\InstagramAPI\Instagram $instagram, $tag)
    {
        $this->instagram = $instagram;
        $this->tag = $tag;
    }

    public function search()
    {
        $locations = $this->instagram->searchFBLocation($this->tag);
        //Случайное местоположение из самых популярных
        $i = array_rand($locations->items);
        $location = $locations->items[$i];

        $id_location = $location->location->pk;

        $feed = $this->instagram->getLocationFeed($id_location);
        //Случайный item
        $item = array_rand($feed->items);

        $this->user_id  = (int)$feed->items[$item]->user->pk;
        $this->media_id = (int)$feed->items[$item]->pk;
    }
}