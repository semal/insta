<?php

namespace App\Tasks\Search;

use Illuminate\Support\Facades\DB;

class SexSearch
{
    public $user_id = 0;
    public $media_id = 0;

    private $instagram;
    private $sex = 0;

    public function __construct(\InstagramAPI\Instagram $instagram, $sex)
    {
        $this->instagram = $instagram;
        switch ($sex)
        {
            case 0:
                $this->sex = 'М';
                break;
            case 1:
                $this->sex = 'Ж';
                break;
        }
    }

    public function search()
    {
        //Получаем список самых популярных имен, из которых будем вести поиск
        $names = DB::table('russian_names')->where('Sex',$this->sex)->where('PeoplesCount','>','900000')->get();
        //Выбираем случайное имя
        $random_name = $names[array_rand($names->all())]->Name;
        //Ищем пользователей
        $list = $this->instagram->searchUsers($random_name);
        //Выбираем случайного из всего списка
        $item = array_rand($list->users);
        //Получаем id найденного пользователя
        $this->user_id = $list->users[$item]->pk;
        //Получаем id случайной записи его ленты
        $feed = $this->instagram->getUserFeed($this->user_id);
        $this->media_id = $feed->items[array_rand($feed->items)]->pk;
    }
}