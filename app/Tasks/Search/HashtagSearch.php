<?php

namespace App\Tasks\Search;

class HashtagSearch
{
    public $user_id = 0;
    public $media_id = 0;

    private $instagram;
    private $tag;

    public function __construct(\InstagramAPI\Instagram $instagram, $tag)
    {
        $this->instagram = $instagram;
        $this->tag = $tag;
    }

    public function search()
    {
        $list = array();
        $i = 0;
        do{
            if(empty($list))
                $list[$i] = $this->instagram->getHashtagFeed($this->tag);
            else
                $list[$i] =  $this->instagram->getHashtagFeed($this->tag,$list[$i-1]->getNextMaxId());
            $i++;
        } while(!is_null($list[$i-1]->getNextMaxId()) && $i < 10);

        //Случайная страница инстаграм
        $page = array_rand($list);
        //Случайная запись
        $item = array_rand($list[$page]->items);

        $this->user_id = $list[$page]->items[$item]->user->pk;
        $this->media_id = $list[$page]->items[$item]->pk;
    }
}