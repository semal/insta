<?php

namespace App\Tasks;

class TaskFactory
{
    private $instagram;
    private $user_id  = 0;
    private $media_id = 0;

    public function __construct(\InstagramAPI\Instagram $instagram, $user_id, $media_id)
    {
        $this->instagram = $instagram;
        $this->user_id   =   $user_id;
        $this->media_id  =  $media_id;
    }

    public function make($type,$account_id = 0)
    {
        //В зависимости от типа задачи создаем объект
        switch ($type)
        {
            case 0:
                return new FollowTask($this->instagram,$this->user_id,$this->media_id,$account_id);
                break;
            case 1:
                return new LikeTask($this->instagram,$this->user_id,$this->media_id,$account_id);
                break;
            case 2:
                return new CommentTask($this->instagram,$this->user_id,$this->media_id,$account_id);
                break;
            case 3:
                return new UnfollowTask($this->instagram,$account_id);
            default:
                return 0;
                break;
        }
    }

}