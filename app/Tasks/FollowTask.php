<?php

namespace App\Tasks;

use App\Account;
use App\CompletedTask;

class FollowTask implements TaskInterface
{

    private $instagram;
    private $user_id  = 0;
    private $media_id = 0;
    private $account_id = 0;

    public function __construct(\InstagramAPI\Instagram $instagram, $user_id, $media_id, $account_id)
    {
        $this->instagram = $instagram;
        $this->user_id   = $user_id;
        $this->media_id  = $media_id;
        $this->account_id = $account_id;
    }

    public function execute()
    {
        $like = new LikeTask($this->instagram,$this->user_id,$this->media_id,$this->account_id);
        $like->execute();
        sleep(rand(4,15));
        $this->instagram->follow($this->user_id);
        $this->save();
    }

    private function save()
    {
        $time = date("Y-m-d H:i:00",time());
        $account = Account::find($this->account_id);
        $completedTask = new CompletedTask([
            'type'    => 0,
            'time'    => $time,
            'user_id' => $this->user_id,
        ]);

        $account->completed_tasks()->save($completedTask);
    }
}