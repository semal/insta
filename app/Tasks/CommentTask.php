<?php

namespace App\Tasks;

use App\Comment;

use App\Account;
use App\CompletedTask;

class CommentTask implements TaskInterface
{
    private $instagram;
    private $user_id  = 0;
    private $media_id = 0;
    private $account_id = 0;
    private $comment = "";

    public function __construct(\InstagramAPI\Instagram $instagram, $user_id, $media_id, $account_id)
    {
        $this->instagram = $instagram;
        $this->user_id   = $user_id;
        $this->media_id  = $media_id;
        $this->account_id = $account_id;
    }

    public function execute()
    {
        if($this->media_id !=0 )
        {
            $media_id = $this->media_id;
        }
        else
        {
            $feed     = $this->instagram->getUserFeed($this->user_id);
            $media_id = $feed->items[array_rand($feed->items)]->pk;
        }

        $comments = Comment::all();

        $comments = $comments->toArray();
        $i = array_rand($comments);
        $this->comment = $comments[$i]['comment'];

        $this->instagram->comment($media_id ,$this->comment);

        $this->save();
    }


    private function save()
    {
        $time = date("Y-m-d H:i:00",time());
        $account = Account::find($this->account_id);

        $completedTask = new CompletedTask([
            'type'     => 0,
            'time'     => $time,
            'user_id'  => $this->user_id,
            'media_id' => $this->media_id,
            'comment'  => $this->comment,
        ]);

        $account->completed_tasks()->save($completedTask);
    }
}