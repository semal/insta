<?php
/**
 * Created by PhpStorm.
 * User: DNS
 * Date: 04.04.2017
 * Time: 15:52
 */

namespace App\Tasks;

use App\Account;
use App\CompletedTask;

class UnfollowTask
{
    private $instagram;
    private $account_id;

    public function __construct(\InstagramAPI\Instagram $instagram, $account_id)
    {
        $this->instagram  = $instagram;
        $this->account_id = $account_id;
    }

    public function execute()
    {
        $list = $this->instagram->getSelfUsersFollowing();
        $key = array_rand($list->users);
        $this->instagram->unfollow($list->users[$key]->pk);
        $this->save();
    }

    public function save()
    {
        $time = date("Y-m-d H:i:00",time());
        $account = Account::find($this->account_id);

        $completedTask = new CompletedTask([
            'type'     => 3,
            'time'     => $time,
        ]);

        $account->completed_tasks()->save($completedTask);
    }

}