<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'username',
        'password',
        'instagram_id',
        'fullname',
        'avatar',
        'following',
        'follower',
        'warning'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function completed_tasks()
    {
        return $this->hasMany(CompletedTask::class);
    }

    public function targets()
    {
        return $this->hasOne(Target::class);
    }

    public function followers()
    {
        return $this->hasMany(Follower::class);
    }
}
